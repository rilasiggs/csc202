from binarytree import build, Node

tokens = ['(', '2', '*', '8', ')', '+', '(', '3', '/', '1', ')']

roots = ["+", "*", "/", "3", "8", "3", "1"] # define roots
root = build(roots) # build tree


def postorder(root):
    stack = [root]
    result = []

    while stack:
            node = stack.pop()
            if node:
                result.append(node.value)
                stack.append(node.left)
                stack.append(node.right)
    return result[::-1]


def preorder(root):
    stack = [root]
    result = []

    while stack:
            node = stack.pop()
            if node:
                result.append(node.value)
                stack.append(node.right)
                stack.append(node.left)
    return result


def inorder(root):
    result = []
    stack = [] 
    node = root

    while node or stack:
        while node:
            stack.append(node)
            node = node.left
        if stack:
            node = stack.pop()
            result.append(node.value)
            node = node.right
    return result

postorder = postorder(root)
preorder = preorder(root)
inorder = inorder(root)

print(root)
print(f"infix: {inorder}\nprefix: {preorder}\npostfix: {postorder}")