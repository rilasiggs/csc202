class PQ:
    """
    >>> pq = new()
    >>> pq.size()
    0
    """
    def __init__(self):
        self.data = []

    def __eq__(self, other):
        return self.data == other.data

    def size(self):
        return len(self.data)

def new():
    """
    >>> pq = new()
    >>> isinstance(pq, PQ)
    True
    """
    return PQ()

def insert(val, pq):
    """
    >>> pq = new()
    >>> pq.size()
    0
    >>> insert(7, pq)
    >>> pq.size()
    1
    >>> insert(9, pq)
    >>> print(pq.data)
    [7, 9]
    >>> insert(6, pq)
    >>> print(pq.data)
    [6, 9, 7]
    >>> insert(1, pq)
    >>> print(pq.data)
    [1, 6, 7, 9]
    >>> insert(4, pq)
    >>> print(pq.data)
    [1, 4, 7, 9, 6]
    >>> insert(0, pq)
    >>> print(pq.data)
    [0, 4, 1, 9, 6, 7]
    """
    pq.data.append(val)
    i = pq.data.index(val) # i is the index of the appended item
    while i > 0 and pq.data[i] < pq.data[(i-1)//2]:
        pq.data[i], pq.data[(i-1)//2] = pq.data[(i-1)//2], pq.data[i]
        i = (i-1)//2 # set p to its parent's index
    # append then swim if parent is greater (creating a min heap) iteratively

def min(pq):
    """
    >>> pq = new()
    >>> insert(7, pq)
    >>> min(pq)
    7
    """
    return pq.data[0]

def dm(pq):
    """
    >>> original = new()
    >>> pq = new()
    >>> pq == original
    True
    >>> insert(7, pq)
    >>> insert(8, pq)
    >>> insert(3, pq)
    >>> insert(9, pq)
    >>> insert(2, pq)
    >>> print(pq.data)
    [2, 3, 7, 9, 8]
    >>> dm(pq)
    >>> print(pq.data)
    [3, 8, 7, 9]
    >>> dm(pq)
    >>> print(pq.data)
    [7, 8, 9]
    >>> insert(9, pq)
    >>> insert(2, pq)
    >>> insert(1, pq)
    >>> insert(4, pq)
    >>> insert(3, pq)
    >>> print(pq.data)
    [1, 3, 2, 7, 8, 9, 4, 9]
    >>> dm(pq)
    >>> dm(pq)
    >>> print(pq.data)
    [3, 7, 4, 9, 8, 9]
    >>> dm(pq)
    >>> print(pq.data)
    [4, 7, 9, 9, 8]
    >>> dm(pq)
    >>> print(pq.data)
    [7, 8, 9, 9]
    >>> dm(pq)
    >>> print(pq.data)
    [8, 9, 9]
    >>> dm(pq)
    >>> print(pq.data)
    [9, 9]
    >>> dm(pq)
    >>> print(pq.data)
    [9]
    >>> dm(pq)
    >>> print(pq.data)
    []
    """
    pq.data.pop(0)
    if pq.data == []: return # if dm() is removing the last entry in the pq, no need to excecute the rest of the function
    pq.data.insert(0, pq.data.pop()) # move the last item to the front
    i = 0 # set current node to the root

    while True: # current node is not a leaf
        try:
            l_child, r_child = 2*i + 1, 2*i + 2 # l_child and r_child are the indexes of i's children
            min_child = l_child
            if pq.data[r_child] <  pq.data[l_child]: # if right child is smaller
                min_child = r_child
    
            if pq.data[i] > pq.data[min_child]:
                pq.data[i], pq.data[min_child] = pq.data[min_child], pq.data[i]
            i = min_child
        except IndexError: # IndexError is thrown if current node is a leaf or only has one child
            try: # if current node has a child
                child = pq.data[2*i + 1] # throws IndexError if current node is a leaf
                if pq.data[i] > pq.data[2*i + 1]: # if current node is greater than its child
                    pq.data[i], pq.data[2*i + 1] = pq.data[2*i + 1], pq.data[i] # swap current node and the child
                break # current node is in its correct location
            except IndexError: break # else current node must be a leaf, and its in correct location


if __name__ == "__main__":
    import doctest
    doctest.testmod()