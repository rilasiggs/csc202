from pq import *
import binarytree as bt


# Let's do a heapsort!
pq = new()
unsorted = [12, -2, 10, 4, 3, 8, 7]
print(f'List to be sorted: {unsorted}')

for i in unsorted: # insert each item into the heap
    insert(i, pq)
print(f'Heap before deleting:\n{bt.build(pq.data)}')

sorted = []
for i in range(len(pq.data)): # retrieve and remove the minimums from the heap
    sorted.append(min(pq))
    dm(pq)
print(f'Sorted list: {sorted}')