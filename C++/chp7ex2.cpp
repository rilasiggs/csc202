#include <iostream>
#include <string>
using namespace std;

void reverse(string my_string){
    int index = my_string.length();
    string new_string;
    while (index >= 0) {
        char letter = my_string[index];
        new_string += letter;
        index = index - 1;
    }
    cout << new_string << endl;
}

int main()
{
    reverse("Strings!");
    return 0;
}