#include <ctime>
#include <cstdlib>
#include <iostream>
using namespace std;

int main()
{
    cout << "RAND_MAX is: " << RAND_MAX << endl;
    cout << "Let's generate 10 random numbers." << endl;
    for (int i = 1; i < 11; i++) {
        cout << "Random number " << i << ": " << rand() % 9 + 1<< endl;
    }

    return 0;
}