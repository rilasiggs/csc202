#include <iostream>
#include <math.h>
using namespace std;

void print_multiples(int n, int high)
{
    int i = 1;
    while (i <= high) {
        cout << n * i << "   ";
        i = i + 1;
    }
    cout << endl;
}

void print_mult_table(int high)
{
    int i = 1;
    while (i <= high) {
        print_multiples(i, i);
        i = i + 1;
    }
}

int main(){
    print_multiples(5, 4);
    print_mult_table(5);
    return 0;
}