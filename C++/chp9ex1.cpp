#include<iostream>
using namespace std;

struct Time {
    int hour, minute;
    double second;
};

void reset(const Time& t1) // does not compile, cannot modify t1 as it is const
{
    // commented out lines 12-14 so that VS code does not show files with compiler errors
    // t1.hour = 0;
    // t1.minute = 0;
    // t1.second = 0;
}

int main(){
    Time time = {12, 5, 35};
    reset(time);
    cout << time.hour << "." << time.minute << "." << time.second << endl;
}