#include<iostream>
using namespace std;

struct Point {
    double x, y;
};

struct Rectangle {
    Point upperLeft;
    double width, height;
};

int findArea(Rectangle rect){
    return rect.height * rect.width;
}

int main()
{
    Rectangle box = {{0, 0}, 12.0, 11.0};
    cout << findArea(box) << endl;
    return 0;

}