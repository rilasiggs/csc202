#include <iostream>
#include <string>
using namespace std;


int countLetters(string my_string, char _char){
    int index = 0;
    int count = 0;

    while (index < my_string.length() - 1){
        if (my_string.find(_char, index) == -1){
            return count;
        } else {
            if (my_string[index] == _char){
                count += 1;
            }
            index += 1;
        }
    }
    return count + 1;
}

int main()
{
    cout << countLetters("Mississippi", 'i') << endl;
    return 0;
}