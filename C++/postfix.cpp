#include <iostream>
#include <string>
#include "LinkedList.h"
using namespace std;


template <class T>
class Stack : public LinkedList<T>
{
  public:
    // constructors
    Stack() = default;
    
    // modifiers
    void push(T item) {
        LinkedList<T>::insert_at_front(item);
    }

    T pop() {
        return LinkedList<T>::remove_from_front();
    }

    bool empty() const {
        return (LinkedList<T>::length() == 0);
    }

    T top() {
        if (LinkedList<T>::head == NULL)
            throw runtime_error("Can't return top item of empty stack!");
        return LinkedList<T>::head->cargo;
    }
};


// Function to evaluate a postfix expression
int evaluatePostfixExpression(const string& expression) {
    Stack<int> stack; // Create a stack to store intermediate results

    for (char c : expression) {
        if (isdigit(c)) {
            int operand = c - '0'; // Convert character to integer operand
            stack.push(operand); // Push operand onto the stack
        } else if (c == '+' || c == '-' || c == '*' || c == '/') {
            // Operator encountered, perform operation on the top two operands in the stack
            int operand1 = stack.pop();
            int operand2 = stack.pop();
            int result;

            switch (c) {
                case '+':
                    result = operand1 + operand2;
                    break;
                case '-':
                    result = operand1 - operand2;
                    break;
                case '*':
                    result = operand1 * operand2;
                    break;
                case '/':
                    result = operand1 / operand2;
                    break;
            }

            stack.push(result); // Push the result back onto the stack
        }
    }

    // The final result will be the only item left in the stack
    return stack.pop();
}

int main() {
    // Read user input
    string postfixExpression;
    cout << "Enter a postfix expression: ";
    getline(cin, postfixExpression); // Save user input to string var postfixExpression

    // Evaluate and print
    int result = evaluatePostfixExpression(postfixExpression); // Evaluate the postfix expression
    cout << "Result: " << result << endl; // Output the result

    return 0;
}