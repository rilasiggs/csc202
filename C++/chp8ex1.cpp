#include<iostream>
using namespace std;

struct Point {
    double x, y;
};

struct Rectangle {
    Point upperLeft;
    double width, height;
};

Point lowerRight(Rectangle rect){
    Point lowerRight = {rect.upperLeft.x + rect.width, rect.upperLeft.y - rect.height};
    return lowerRight;
}

int main()
{
    Rectangle box = {{0, 0}, 12.0, 11.0};
    Point final = lowerRight(box);

    cout << "lower right point: (" << final.x << "," << final.y << ")" << endl;
    return 0;

}