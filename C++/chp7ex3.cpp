#include <iostream>
#include <string>
using namespace std;

void countLetters(string my_string, char _char){
    string state = my_string;
    int count = 0;
    int index = 0;

    while (index < state.length()) {
        if (state[index] == _char) {
            count = count + 1;
        }
        index = index + 1;
    }
    cout << count << endl;
}

int main()
{
    countLetters("Mississippi", 'i');
    return 0;
}