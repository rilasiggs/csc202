#include <iostream>
#include <cctype>
using namespace std;

string stringToUpper(string my_string){
    string new_string;
    for (int i = 0; i < my_string.length(); i++) { //uppercase each character individually bc toupper() only takes a char
        new_string += toupper(my_string[i]);
    }
    return new_string;
}

string stringToLower(string my_string){
    string new_string;
    for (int i = 0; i < my_string.length(); i++) { //lowercase each character individually bc tolower() only takes a char
        new_string += tolower(my_string[i]);
    }
    return new_string;
}

int main()
{
    cout << stringToUpper("Upperize") << endl; cout << stringToLower("Lowerize") << endl;
    return 0;
}
